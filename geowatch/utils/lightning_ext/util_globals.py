# Autogenerated via:
# python ~/code/watch/dev/maintain/mirror_package_geowatch.py
def __getattr__(key):
    import watch.utils.lightning_ext.util_globals as mirror
    return getattr(mirror, key)