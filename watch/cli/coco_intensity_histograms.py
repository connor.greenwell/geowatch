"""
Deprecated and moved to coco_spectra
"""
from watch.cli.coco_spectra import *  # NOQA
from watch.cli.coco_spectra import __config__  # NOQA
from watch.cli.coco_spectra import main


if __name__ == '__main__':
    main()
