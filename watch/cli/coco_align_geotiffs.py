"""
DEPRECATED: Use coco_align instead
"""
# flake8: noqa
from watch.cli.coco_align import *
from watch.cli.coco_align import __config__
from watch.cli.coco_align import main


if __name__ == '__main__':
    main()
