from watch.cli.__main__ import main

if __name__ == '__main__':
    """
    CommandLine:
        python -m watch --help
    """
    main()
